package main.java;

import com.sun.xacml.attr.AttributeFactory;
import com.sun.xacml.attr.AttributeFactoryProxy;
import com.sun.xacml.attr.BaseAttributeFactory;
import com.sun.xacml.attr.StandardAttributeFactory;

public class Program {

    public static void main(String[] args) {

        StandardAttributeFactory standardFactory =
                StandardAttributeFactory.getFactory();
        final BaseAttributeFactory newFactory =
                new BaseAttributeFactory(standardFactory.getStandardDatatypes());
        // modify the contents of the new factory
        AttributeFactory.setDefaultFactory(new AttributeFactoryProxy() {
            public AttributeFactory getFactory() {
                return newFactory;
            }
        });
        System.out.println("Hello world");
    }
}
